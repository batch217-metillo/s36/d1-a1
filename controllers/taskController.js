const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}

		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updatedTask;
			}
		})
	})
}

/*
	ACTIVITY START
*/

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	});
}


// Using findById
module.exports.updateStatus1 = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}

		result.status = newStatus.status;
		return result.save().then((updatedStatus, saveError) => {
			if(saveError){
				console.log(saveError);
				return false;
			}else{
				return updatedStatus;
			}
		})
	})
}

// Using findByIdAndUpdate
/*module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findByIdAndUpdate(taskId, newStatus).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}

		result.status = newStatus.status;
		return result.save().then((updatedStatus, saveError) => {
			if(saveError){
				console.log(saveError);
				return false;
			}else {
				return updatedStatus;
			}
		})
	})
}*/

/*
	ACTIVITY END
*/
