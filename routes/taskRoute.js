// It contains all endpoints for our application
const express = require("express");
const router = express.Router();
// .Router() is a module in express
const taskController = require("../controllers/taskController.js")

// [SECTION] Routes
// It is responsible for defining or creating endpoints
// All the business logic is done in the controller

// localhost:3001/tasks/viewTasks
router.get("/viewTasks", (req, res) => {
	// Invokes the "getAllTasks function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// localhost:3001/tasks/addNewTask
router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// params is located sa URL, id sa endpoint ay id number 
// localhost:3001/tasks/deleteTask/idNumber
router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then( 
		resultFromController => res.send(resultFromController));
})

// localhost:3001/tasks/updateTask/idNumber
router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

/*
	ACTIVIY START
*/

// Getting a specific task
// localhost:3001/tasks/specificTask/idNumber
router.get("/specificTask/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Updating the status of a task (using params.status)
// localhost:3001/tasks/idNumber/status
/*router.put("/:id/:status", (req, res) => {
	taskController.updateStatus(req.params.id, req.params.status).then(resultFromController => res.send(resultFromController));
})*/

// Updating the status of a task (using req.body)
// localhost:3001/tasks/idNumber/status
router.put("/:id/complete", (req, res) => {
	taskController.updateStatus1(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

/*
	ACTIVITY END
*/

// module is responsible for transferring file or code to other files
module.exports = router;